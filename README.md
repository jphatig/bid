LEVANTAR PROYECTO EN DESARROLLO
---

PROCEDIMIENTO

NOTA: Si se usa windows se debe cambiar todos los archivos con formato de fin de linea de `CLRF` a `LF`

- Crear los siguientes archivos en base a cada `.sample`(Considerar que las credenciales de base de datos deben coincidir entre ellos), además se debe cambiar la IP `0.0.0.0` a la IP del equipo local (Ej. 172.18.16.53) en donde sea requerido:
    - `backend/.envs/fastapi.env`
    - `backend/.envs/database.env`
    - `frontend/.env.local`
    - `sso/.env`
    - `postgres.env`
    - `pgadmin.env`
 
- Instalar NodeJS 18 y ejecutar `npm install` dentro del directorio `frontend`
- Levantar los contenedores: `docker compose up --build`. 

- Acceder a Keycloak: http://IP:8002 (Ej: http://172.18.16.53:8002)
    - En el reino `master` 
        - Configurar el correo del administrador:

            <img src=".docs/keycloak-update-master-admin-email.png" width="700"/>
        
        - Agregar la dirección IP utilizada (Ej. 172.18.16.53) con el puerto 3005 en las tres listas de: `Valid redirect URIs`, `Valid post logout redirect URIs` y `Web Origins`:

            <img src=".docs/keycloak-client-redirect.png" width="700"/>

    - En el reino `bid`: 
        - Crear el usuario `admin`:

            <img src=".docs/keycloak-create-bid-admin.png" width="700"/>

        - Cambiar la contraseña del mismo usuario a `admin` (O la establecida en `sso/.env`)

            <img src=".docs/keycloak-update-pass-bid-admin.png" width="700"/>

        - Agregar el rol `manage-users` al usuario:

            <img src=".docs/keycloak-roles-bid-admin.png" width="700"/>

- Agregar usuarios para demostración ejecutando: 

    `docker exec -u root -it bid-backend python /opt/app-root/src/app/demo.py`

    - O utilizar el API del backend: http://IP:8001 (Ej: http://172.18.16.53:8001):
        - [POST] /api/v1/bidder
        - [POST] /api/v1/bid

- Acceder a mailhog http://IP:8020 (Ej: http://172.18.16.53:8020) y resolver el cambio de clave de los usuarios `DEMO 000` y `DEMO 001` mediante el enlace de confirmación incluidos en los correos entrantes. 
- Acceder al frontend http://IP:3005 (Ej:http://172.18.16.53:3005) en dos navegadores (O en el mismo utilizando navegación privada)
- Iniciar sesión con ambos usuarios y probar el funcionamiento del WebSocket