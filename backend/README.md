Agregar versiones en Alembic

- Acceder al contenedor: `docker exec -u root -it bid-backend sh`
    
    - Acceder al directorio: `cd app/`
    - Crear la versión: `alembic revision -m "..." --autogenerate`

- Corregir permisos (Ya que al generar la versión dentro del contenedor los permisos quedan como root)

    `sudo chown -R $(id -u):$(id -g) app/alembic/versions/`