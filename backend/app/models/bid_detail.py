from __future__ import annotations

from datetime import datetime
from uuid import UUID

import sqlalchemy as sa
from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models.utils.base import Base


class BidDetailModel(Base):
    __tablename__ = "bid_detail"

    bid_id: Mapped[UUID] = mapped_column(ForeignKey("bid.id"))
    bidder_id: Mapped[UUID] = mapped_column(ForeignKey("bidder.id"))
    value: Mapped[float]
    created_at: Mapped[datetime] = mapped_column(server_default=sa.func.now())

    bid: Mapped["BidModel"] = relationship(lazy="noload")
    bidder: Mapped["BidderModel"] = relationship(lazy="noload")


from app.models.bid import BidModel
from app.models.bidder import BidderModel
