from uuid import UUID

from sqlalchemy import MetaData, text
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy.types import Uuid

from app.config.database.local import metadata


class Base(DeclarativeBase):
    metadata: MetaData = metadata

    id: Mapped[UUID] = mapped_column(
        Uuid,
        server_default=text("gen_random_uuid()"),
        primary_key=True,
        index=True,
    )
