from __future__ import annotations

from typing import List
from uuid import UUID

from sqlalchemy.orm import Mapped, relationship

from app.models.utils.base import Base


class BidderModel(Base):
    __tablename__ = "bidder"

    name: Mapped[str]
    user_id: Mapped[UUID]

    won_bids: Mapped[List["BidModel"]] = relationship(back_populates="winner")
    bids: Mapped[List["BidBidderModel"]] = relationship(back_populates="bidder")


from app.models.bid import BidModel
from app.models.bid_bidder import BidBidderModel
