from __future__ import annotations

from datetime import datetime
from typing import List

import sqlalchemy as sa
from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models.utils.base import Base


class BidModel(Base):
    __tablename__ = "bid"

    title: Mapped[str]
    value: Mapped[float]
    start_date: Mapped[datetime]
    end_date: Mapped[datetime]
    winner_id: Mapped[int] = mapped_column(ForeignKey("bidder.id"), nullable=True)
    won_value: Mapped[float] = mapped_column(nullable=True)
    created_at: Mapped[datetime] = mapped_column(server_default=sa.func.now())

    winner: Mapped["BidderModel"] = relationship()
    details: Mapped[List["BidDetailModel"]] = relationship(back_populates="bid")


from app.models.bid_detail import BidDetailModel
from app.models.bidder import BidderModel
