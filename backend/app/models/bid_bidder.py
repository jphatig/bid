from __future__ import annotations

from uuid import UUID

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models.utils.base import Base


class BidBidderModel(Base):
    __tablename__ = "bid_bidder"

    bid_id: Mapped[UUID] = mapped_column(ForeignKey("bid.id"))
    bidder_id: Mapped[UUID] = mapped_column(ForeignKey("bidder.id"))

    bid: Mapped["BidModel"] = relationship()
    bidder: Mapped["BidderModel"] = relationship(back_populates="bids")


from app.models.bid import BidModel
from app.models.bidder import BidderModel
