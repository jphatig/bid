"""Ruta para sincronización de proveedores"""

from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends, WebSocket, status

from app.manager.bid import BidManager
from app.schemas.bid import (
    BidCreateSchema,
    BidOptionalReadSchema,
    BidReadSchema,
    BidUpdateSchema,
)

router = APIRouter(prefix="/bid", tags=["Bid"])


@router.get(
    path="/{record_id}",
    status_code=status.HTTP_200_OK,
    response_model=BidOptionalReadSchema,
)
def get_bid(
    record_id: UUID,
    manager: BidManager = Depends(),
):
    return manager.get(record_id)


@router.get(
    path="/{record_id}/details",
    status_code=status.HTTP_200_OK,
    response_model=BidOptionalReadSchema,
)
def get_bid_details(
    record_id: UUID,
    manager: BidManager = Depends(),
):
    return manager.get_details(record_id)


@router.get(
    path="",
    status_code=status.HTTP_200_OK,
    response_model=List[BidReadSchema],
)
def get_bids(
    manager: BidManager = Depends(),
):
    return manager.get_all()


@router.post(
    path="",
    status_code=status.HTTP_201_CREATED,
    response_model=BidOptionalReadSchema,
)
def add_bid(
    data: BidCreateSchema,
    manager: BidManager = Depends(),
):
    record = manager.create(data)
    return record


@router.put(
    path="/{record_id}",
    status_code=status.HTTP_200_OK,
    response_model=BidOptionalReadSchema,
)
def update_bid(
    record_id: UUID,
    data: BidUpdateSchema,
    manager: BidManager = Depends(),
):
    return manager.update(record_id, data)


@router.delete(
    path="/{record_id}",
    status_code=status.HTTP_200_OK,
)
def remove_bid(
    record_id: UUID,
    manager: BidManager = Depends(),
):
    return manager.remove(record_id)


@router.websocket("/ws")
async def websocket_endpoint(
    websocket: WebSocket,
    manager: BidManager = Depends(),
):
    await manager.on_socket(websocket)
