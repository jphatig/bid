"""Ruta para sincronización de proveedores"""

from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends, status

from app.manager.bidder import BidderManager
from app.schemas.bidder import (
    BidderCreateSchemaWithKeycloak,
    BidderOptionalReadSchema,
    BidderReadSchema,
    BidderUpdateSchema,
)
from app.utils.token import get_access_token

router = APIRouter(prefix="/bidder", tags=["Bidder"])


@router.get(
    path="/me", status_code=status.HTTP_200_OK, response_model=BidderOptionalReadSchema
)
def get_bidder_by_token(
    manager: BidderManager = Depends(),
    token: str = Depends(get_access_token),
):
    return manager.get_by_token(token)


@router.get(
    path="/{record_id}",
    status_code=status.HTTP_200_OK,
    response_model=BidderOptionalReadSchema,
)
def get_bidder(
    record_id: UUID,
    manager: BidderManager = Depends(),
):
    return manager.get(record_id)


@router.get(
    path="/{record_id}/bids",
    status_code=status.HTTP_200_OK,
    response_model=BidderOptionalReadSchema,
)
def get_bidder_bids(
    record_id: UUID,
    manager: BidderManager = Depends(),
):
    return manager.get_bids(record_id)


@router.get(
    path="",
    status_code=status.HTTP_200_OK,
    response_model=List[BidderReadSchema],
)
def get_bidders(
    manager: BidderManager = Depends(),
):
    return manager.get_all()


@router.post(
    path="",
    status_code=status.HTTP_200_OK,
    response_model=BidderOptionalReadSchema,
)
def add_bidder(
    data: BidderCreateSchemaWithKeycloak,
    manager: BidderManager = Depends(),
):
    return manager.create(data)


@router.post(
    path="/{record_id}/bid/{bid_id}/join",
    status_code=status.HTTP_200_OK,
    response_model=BidderOptionalReadSchema,
)
def join_bid(
    record_id: UUID,
    bid_id: UUID,
    manager: BidderManager = Depends(),
):
    return manager.join_bid(record_id, bid_id)


@router.put(
    path="/{record_id}",
    status_code=status.HTTP_200_OK,
    response_model=BidderOptionalReadSchema,
)
def update_bidder(
    record_id: UUID,
    data: BidderUpdateSchema,
    manager: BidderManager = Depends(),
):
    return manager.update(record_id, data)


@router.delete(
    path="/{record_id}",
    status_code=status.HTTP_200_OK,
)
def remove_bidder(
    record_id: UUID,
    manager: BidderManager = Depends(),
):
    return manager.remove(record_id)
