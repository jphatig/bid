from app.routes import bid, bidder

routes = [
    bidder.router,
    bid.router,
]
