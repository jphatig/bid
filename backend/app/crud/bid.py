from uuid import UUID

from sqlalchemy import select
from sqlalchemy.orm import subqueryload

from app.crud.utils.base import CrudBase
from app.models.bid import BidModel
from app.models.bid_detail import BidDetailModel


class BidCrud(CrudBase):

    def get_lowest_bid(self, bid_id: UUID):
        query = (
            select(BidDetailModel)
            .where(
                BidDetailModel.bid_id == bid_id,
            )
            .order_by(
                BidDetailModel.value.asc(),
            )
            .limit(1)
        )
        return self.session.scalars(query).one_or_none()

    def get_bid_details(self, bid_id: UUID):
        query = (
            select(BidModel)
            .options(
                subqueryload(BidModel.details),  # type: ignore
            )
            .where(BidModel.id == bid_id)
        )
        return self.session.scalars(query).one_or_none()
