"""Base CRUD definitions"""

"""Base CRUD definitions"""

from typing import Any, List, Sequence, Type, TypeVar

from fastapi import Depends
from pydantic import BaseModel
from sqlalchemy import delete, insert, select, update
from sqlalchemy.orm import InstrumentedAttribute, Session, load_only

# project files
from app.config.database.local import Base, get_session
from app.crud import CRUDS

ModelType = TypeVar("ModelType", bound=Base)


class DatabaseRepository(CRUDS):
    """Base CRUD definitions"""

    def __init__(
        self,
        session: Session = Depends(get_session),
    ):
        super().__init__(session)

    def get(
        self,
        model: ModelType,
        record_id: Any,
        columns: List[InstrumentedAttribute] = [],
    ) -> ModelType | None:
        """Get record by id"""
        query = select(model).where(model.id == record_id)
        if columns:
            query = query.options(load_only(*columns))
        return self.session.scalars(query).one_or_none()

    def get_all(
        self,
        model: Type[ModelType],
        columns: List[InstrumentedAttribute] = [],
        skip: int | None = 0,
        limit: int | None = 100,
    ) -> Sequence[ModelType]:
        """Get record list"""
        query = select(model)
        if columns:
            query = query.options(load_only(*columns))
        if skip and limit:
            query = query.offset(skip).limit(limit)
        return self.session.scalars(select(model)).all()

    def insert(
        self,
        model: Type[ModelType],
        data: BaseModel,
        returning: bool = False,
    ) -> ModelType | None:
        """Create record"""
        query = insert(model).values(data.model_dump(exclude_unset=True))
        if returning:
            query = query.returning(model)
        record = self.session.scalars(query).one_or_none()
        self.session.commit()
        return record

    def update(
        self,
        model: ModelType,
        record_id: Any,
        data: BaseModel,
        returning: bool = False,
    ) -> ModelType | None:
        """Update record"""
        query = (
            update(model)
            .where(model.id == record_id)
            .values(**data.model_dump(exclude_unset=True))
        )
        record: ModelType | None = None
        if returning:
            query = query.returning(model)
            record = self.session.scalars(query).one_or_none()
        self.session.execute(query)
        self.session.commit()
        return record

    def remove(self, model: Type[ModelType], record_id: Any):
        """Remove record"""
        query = delete(model).where(model.id == record_id)
        self.session.execute(query)
        self.session.commit()
