from app.crud.bid import BidCrud
from app.crud.bidder import BidderCrud

class CRUDS(
    BidCrud,
    BidderCrud
):
    pass
