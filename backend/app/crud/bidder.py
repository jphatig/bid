from uuid import UUID

from sqlalchemy import select
from sqlalchemy.orm import subqueryload

from app.crud.utils.base import CrudBase
from app.models.bidder import BidderModel


class BidderCrud(CrudBase):

    def get_bidder_bids(self, bidder_id: UUID):
        query = (
            select(BidderModel)
            .options(
                subqueryload(BidderModel.bids),  # type: ignore
            )
            .where(BidderModel.id == bidder_id)
        )
        return self.session.scalars(query).one_or_none()
