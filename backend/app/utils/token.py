from fastapi import Request


def get_access_token(request: Request):
    header = request.headers.get("Authorization")
    if not header:
        return
    return header.split("Bearer ")[1]
