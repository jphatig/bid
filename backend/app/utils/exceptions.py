from typing import Any, Dict, Optional

from fastapi import HTTPException

from app.schemas.utils.error import ErrorContentSchema


class DetailHttpException(HTTPException):
    def __init__(
        self,
        status_code: int,
        detail: Optional[ErrorContentSchema] = None,
        code: Optional[str] = None,
        message: Optional[str] = None,
        headers: Optional[Dict[str, Any]] = None,
    ) -> None:
        if (not code or not message) and not detail:
            raise HTTPException(500, "Invalid detail http exception")
        body = detail.model_dump() if detail else {"code": code, "message": message}
        super().__init__(
            status_code=status_code,
            detail=body,
            headers=headers,
        )
