from typing import List
from uuid import UUID, uuid4

import jwt
import requests
from requests import Response

from app.config.messages import MESSAGES
from app.config.settings import KEYCLOAK
from app.schemas.utils.keycloak import (
    ACTION,
    KeycloakCredentials,
    KeycloakUser,
    KeycloakUserCreate,
)
from app.utils.exceptions import DetailHttpException


class KeycloakUtils:

    token: str

    def get_user(self, token: str):
        jwks_client = jwt.PyJWKClient(uri=KEYCLOAK.CERTS_URL)
        signing_key = jwks_client.get_signing_key_from_jwt(token)
        payload = jwt.decode(
            token,
            signing_key.key,
            algorithms=KEYCLOAK.JTW_ALGORITHMS,
            audience=KEYCLOAK.JWT_AUDIENCE,
        )
        return KeycloakUser(
            id=payload.get("sub"),
            firstName=payload.get("given_name"),
            lastName=payload.get("family_name"),
            username=payload.get("preferred_username"),
            email=payload.get("email"),
        )

    def add_user(self, data: KeycloakUserCreate):
        self.token = self._get_admin_token()
        self._create_user(data)
        user = self._get_user(data.username)
        self._execute_action_email(user.id, ["UPDATE_PASSWORD"])
        return user

    def _get_admin_token(self) -> str:
        data = {
            "client_secret": KEYCLOAK.CLIENT_SECRET,
            "client_id": KEYCLOAK.CLIENT_ID,
            "grant_type": KEYCLOAK.GRANT_TYPE,
            "username": KEYCLOAK.USERNAME,
            "password": KEYCLOAK.PASSWORD,
        }
        response = requests.post(
            KEYCLOAK.TOKEN_URL,
            data=data,
            timeout=KEYCLOAK.TIMEOUT,
            verify=not KEYCLOAK.DISABLE_SSL,
        )
        if response.status_code in range(200, 300):
            return response.json().get("access_token")
        sso_error = response.text
        msg = MESSAGES.SSO_TOKEN_ERROR
        raise DetailHttpException(
            500,
            code=msg.code,
            message=f"{msg.message}: {sso_error}",
        )

    def _set_random_password(self, data: KeycloakUserCreate):
        def parse(x: KeycloakCredentials):
            x.value = str(uuid4())
            return x

        data.credentials = [parse(x) for x in data.credentials]
        return data

    def _execute_action_email(self, user_id: UUID, actions: List[ACTION]):
        headers = {
            "Authorization": f"Bearer {self.token}",
        }
        url = KEYCLOAK.EXECUTE_ACTIONS_URL.replace("{user_id}", str(user_id))
        response = requests.put(
            url,
            json=actions,
            timeout=KEYCLOAK.TIMEOUT,
            headers=headers,
            verify=not KEYCLOAK.DISABLE_SSL,
        )
        if response.status_code in range(200, 300):
            return
        sso_error = response.text
        msg = MESSAGES.SSO_EXECUTE_ACTION_ERROR
        raise DetailHttpException(
            500,
            code=msg.code,
            message=f"{msg.message}: {sso_error}",
        )

    def _create_user(self, data: KeycloakUserCreate):
        data = self._set_random_password(data)
        headers = {"Authorization": f"Bearer {self.token}"}
        response = requests.post(
            KEYCLOAK.USERS_URL,
            json=data.model_dump(),
            timeout=KEYCLOAK.TIMEOUT,
            headers=headers,
            verify=not KEYCLOAK.DISABLE_SSL,
        )
        if response.status_code in range(200, 300):
            return
        sso_error = response.text
        msg = MESSAGES.SSO_CREATE_USER_ERROR
        raise DetailHttpException(
            500,
            code=msg.code,
            message=f"{msg.message}: {sso_error}",
        )

    def _parse_user_response(self, response: Response) -> KeycloakUser:
        data = response.json()
        if not data:
            raise DetailHttpException(400, MESSAGES.SSO_GET_USER_ERROR)
        return KeycloakUser(**data[0])

    def _get_user(self, username: str):
        headers = {"Authorization": f"Bearer {self.token}"}
        params = f"exact=true&username={username}"
        url = f"{KEYCLOAK.USERS_URL}?{params}"
        response = requests.get(
            url,
            timeout=KEYCLOAK.TIMEOUT,
            headers=headers,
            verify=not KEYCLOAK.DISABLE_SSL,
        )
        if response.status_code in range(200, 300):
            return self._parse_user_response(response)
        sso_error = response.text
        msg = MESSAGES.SSO_CREATE_USER_ERROR
        raise DetailHttpException(
            500,
            code=msg.code,
            message=f"{msg.message}: {sso_error}",
        )
