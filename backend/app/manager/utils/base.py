from fastapi import Depends

from app.crud.utils.database import DatabaseRepository


class ManagerBase:

    def __init__(self, database: DatabaseRepository = Depends()):
        self.database = database
