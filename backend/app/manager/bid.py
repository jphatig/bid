from datetime import datetime
from typing import Any, Dict
from uuid import UUID

from fastapi import WebSocket, WebSocketDisconnect

from app.manager.socket_connection import SocketConnectionManager
from app.manager.utils.base import ManagerBase
from app.models import bid_detail
from app.models.bid import BidModel
from app.models.bid_detail import BidDetailModel
from app.schemas.bid import BidCreateSchema, BidUpdateSchema
from app.schemas.bid_detail import (
    BidDetailCreateSchema,
    BidDetailSchema,
    BidSocketMessageSchema,
)

socket_manager = SocketConnectionManager()


class BidManager(ManagerBase):

    def get(self, record_id: UUID):
        return self.database.get(BidModel, record_id)

    def get_all(self):
        return self.database.get_all(BidModel)

    def create(self, data: BidCreateSchema):
        data.created_at = datetime.now()
        return self.database.insert(BidModel, data=data)

    def update(self, record_id: UUID, data: BidUpdateSchema):
        return self.database.update(BidModel, record_id, data)

    def remove(self, record_id: UUID):
        return self.database.remove(BidModel, record_id)

    def create_detail(self, data: BidDetailCreateSchema):
        return self.database.insert(BidDetailModel, data=data)

    def get_details(self, record_id: UUID):
        return self.database.get_bid_details(record_id)

    async def on_socket(self, websocket: WebSocket):
        await socket_manager.connect(websocket)
        try:
            while True:
                data: Dict[str, Any] = await websocket.receive_json()
                detail: Dict[str, Any] = data.get("detail") or {}
                data.update({"detail": {**detail, "created_at": datetime.now()}})
                message = BidSocketMessageSchema(**data)
                bid_detail = message.detail
                self.create_detail(bid_detail)
                lower_bid = self.database.get_lowest_bid(bid_detail.bid_id)
                if not lower_bid:
                    continue
                bid_detail = BidDetailSchema(
                    id=lower_bid.id,
                    bid_id=lower_bid.bid_id,
                    bidder_id=lower_bid.bidder_id,
                    value=lower_bid.value,
                    created_at=lower_bid.created_at,
                )
                await socket_manager.broadcast(bid_detail.model_dump_json())
        except WebSocketDisconnect:
            socket_manager.disconnect(websocket)
