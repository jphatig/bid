from uuid import UUID

from fastapi import Depends

from app.crud.utils.database import DatabaseRepository
from app.manager.utils.base import ManagerBase
from app.models.bid_detail import BidDetailModel
from app.schemas.bid_detail import BidDetailCreateSchema, BidDetailUpdateSchema


class BidDetailManager(ManagerBase):

    def __init__(self, database: DatabaseRepository = Depends()):
        self.database = database

    def get(self, record_id: UUID):
        return self.database.get(BidDetailModel, record_id)

    def get_all(self):
        return self.database.get_all(BidDetailModel)

    def create(self, data: BidDetailCreateSchema):
        return self.database.insert(BidDetailModel, data=data)

    def update(self, record_id: UUID, data: BidDetailUpdateSchema):
        return self.database.update(BidDetailModel, record_id, data)

    def remove(self, record_id: UUID):
        return self.database.remove(BidDetailModel, record_id)
