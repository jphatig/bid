from uuid import UUID

from fastapi import Depends
from sqlalchemy import select

from app.crud.utils.database import DatabaseRepository
from app.manager.utils.base import ManagerBase
from app.models.bid_bidder import BidBidderModel
from app.models.bidder import BidderModel
from app.schemas.bid_bidder import BidBidderCreateSchema
from app.schemas.bidder import (
    BidderCreateSchema,
    BidderCreateSchemaWithKeycloak,
    BidderUpdateSchema,
)
from app.schemas.utils.keycloak import KeycloakUserCreate
from app.utils.keycloak import KeycloakUtils


class BidderManager(ManagerBase):

    def __init__(self, database: DatabaseRepository = Depends()):
        self.database = database

    def get(self, record_id: UUID):
        return self.database.get(BidderModel, record_id)

    def get_by_token(self, token: str):
        keycloak = KeycloakUtils()
        user = keycloak.get_user(token)
        query = select(BidderModel).where(BidderModel.user_id == user.id)
        return self.database.session.scalars(query).one_or_none()

    def get_bids(self, record_id: UUID):
        return self.database.get_bidder_bids(record_id)

    def get_all(self):
        return self.database.get_all(BidderModel)

    def create(self, data: BidderCreateSchemaWithKeycloak):
        keycloak = KeycloakUtils()
        keycloak_user = keycloak.add_user(
            KeycloakUserCreate(**data.keycloak_user.model_dump())
        )
        data.user_id = keycloak_user.id
        return self.database.insert(
            BidderModel,
            data=BidderCreateSchema(**data.model_dump()),
        )

    def update(self, record_id: UUID, data: BidderUpdateSchema):
        return self.database.update(BidderModel, record_id, data)

    def remove(self, record_id: UUID):
        return self.database.remove(BidderModel, record_id)

    def join_bid(self, record_id: UUID, bid_id: UUID):
        return self.database.insert(
            BidBidderModel,
            BidBidderCreateSchema(
                bidder_id=record_id,
                bid_id=bid_id,
            ),
        )
