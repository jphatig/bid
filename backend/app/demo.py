import requests

API = f"http://localhost:8080/api/v1"


def create_bidder(data):
    requests.post(f"{API}/bidder", json=data, verify=False)


def create_bid(data):
    requests.post(f"{API}/bid", json=data, verify=False)


create_bid(
    {
        "title": "BID 001",
        "value": 20000,
        "start_date": "2024-04-17T17:17:40.034Z",
        "end_date": "2025-04-17T17:17:40.034Z",
        "created_at": "2024-04-17T17:17:40.034Z",
    }
)
for i in range(0, 2):
    create_bidder(
        {
            "name": f"DEMO 00{i}",
            "keycloak_user": {
                "username": f"demo00{i}",
                "email": f"demo00{i}@bid.com",
                "firstName": "demo",
                "lastName": f"00{i}",
            },
        }
    )
