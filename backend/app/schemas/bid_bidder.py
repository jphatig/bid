from __future__ import annotations

from uuid import UUID

from pydantic import BaseModel
from pydantic_partial.partial import PartialModelMixin


class BidBidderBaseSchema(PartialModelMixin, BaseModel):
    bid_id: UUID
    bidder_id: UUID


class BidBidderSchema(BidBidderBaseSchema):
    id: UUID


class BidBidderReadSchema(BidBidderSchema):
    pass


BidBidderOptionalReadSchema = BidBidderReadSchema | None


class BidBidderCreateSchema(BidBidderBaseSchema):
    pass


BidBidderPartialSchema = BidBidderSchema.as_partial()
