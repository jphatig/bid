from __future__ import annotations

from typing import Optional
from uuid import UUID

from pydantic import BaseModel
from pydantic_partial.partial import PartialModelMixin

from app.schemas.utils.keycloak import KeycloakUserCreateSimple


class BidderBaseSchema(PartialModelMixin, BaseModel):
    name: str


class BidderRelationSchema(BaseModel):
    pass


class BidderSchema(BidderBaseSchema, BidderRelationSchema):
    id: UUID
    user_id: UUID


class BidderReadSchema(BidderSchema):
    pass


BidderOptionalReadSchema = BidderReadSchema | None


class BidderCreateSchema(BidderBaseSchema):
    user_id: UUID | None = None


class BidderUpdateSchema(BidderBaseSchema):
    name: Optional[str] = None
    user_id: Optional[UUID] = None


BidderPartialSchema = BidderSchema.as_partial()


class BidderCreateSchemaWithKeycloak(BidderCreateSchema):

    keycloak_user: KeycloakUserCreateSimple
