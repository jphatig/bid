from typing import List, Literal
from uuid import UUID

from pydantic import BaseModel

ACTION = Literal["VERIFY_EMAIL", "UPDATE_PASSWORD"]


class KeycloakCredentials(BaseModel):
    temporary: bool = True
    type: Literal["password"] = "password"
    value: str | None = None


class KeycloakUserBase(BaseModel):
    username: str
    email: str
    firstName: str | None = None
    lastName: str | None = None


class KeycloakUser(KeycloakUserBase):
    id: UUID


class KeycloakUserCreateSimple(KeycloakUserBase):
    pass


class KeycloakUserCreate(KeycloakUserCreateSimple):
    enabled: bool = True
    emailVerified: bool = False
    credentials: List[KeycloakCredentials] = [KeycloakCredentials()]
    requiredActions: List[Literal[ACTION]] = ["VERIFY_EMAIL", "UPDATE_PASSWORD"]
