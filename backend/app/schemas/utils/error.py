"""Base schema definition"""

from pydantic import BaseModel


class ErrorContentSchema(BaseModel):
    """Error content schema definition"""

    code: str
    message: str
