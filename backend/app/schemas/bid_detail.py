from __future__ import annotations

from datetime import datetime
from uuid import UUID

from pydantic import BaseModel
from pydantic_partial.partial import PartialModelMixin


class BidDetailBaseSchema(PartialModelMixin, BaseModel):
    bid_id: UUID
    bidder_id: UUID
    value: float
    created_at: datetime


class BidDetailRelationSchema(BaseModel):
    pass


class BidDetailSchema(BidDetailBaseSchema, BidDetailRelationSchema):
    id: UUID


class BidDetailReadSchema(BidDetailSchema):
    pass


BidDetailOptionalReadSchema = BidDetailReadSchema | None


class BidDetailCreateSchema(BidDetailBaseSchema):
    pass


class BidDetailUpdateSchema(BidDetailBaseSchema):
    bid_id: UUID | None = None
    bidder_id: UUID | None = None
    value: float | None = None
    created_at: datetime | None = None


BidDetailPartialSchema = BidDetailSchema.as_partial()


class BidSocketMessageSchema(BaseModel):
    token: str
    detail: BidDetailCreateSchema
