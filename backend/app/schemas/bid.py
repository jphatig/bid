from __future__ import annotations

from datetime import datetime
from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel
from pydantic_partial.partial import PartialModelMixin

from app.schemas.bid_detail import BidDetailSchema


class BidBaseSchema(PartialModelMixin, BaseModel):
    title: str
    value: float
    start_date: datetime
    end_date: datetime
    created_at: datetime


class BidRelationSchema(BaseModel):
    details: List[BidDetailSchema] = []


class BidSchema(BidBaseSchema, BidRelationSchema):
    id: UUID
    winner_id: UUID | None = None
    won_value: float | None = None


class BidReadSchema(BidSchema):
    pass


BidOptionalReadSchema = BidReadSchema | None


class BidCreateSchema(BidBaseSchema):
    pass


class BidUpdateSchema(BidBaseSchema):
    title: Optional[str] = None
    value: float | None = None
    start_date: datetime | None = None
    end_date: datetime | None = None
    created_at: datetime | None = None


BidPartialSchema = BidSchema.as_partial()
