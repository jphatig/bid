"""Main FastAPI file"""

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import RedirectResponse

from app.config.settings import API
from app.routes import routes

app = FastAPI(title=f"{API.TITLE}")

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/", include_in_schema=False)
async def main():
    """Main redirect to docs"""
    return RedirectResponse("/docs")


for route in routes:
    app.include_router(router=route, prefix=API.PREFIX)
