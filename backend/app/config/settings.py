# type: ignore
"""Database sesttings"""


from typing import List

from decouple import config


class DATABASE:

    TYPE: str = config("DATABASE_TYPE")
    USER: str = config("DATABASE_USER")
    PASSWORD: str = config("DATABASE_PASSWORD")
    HOST: str = config("DATABASE_HOST")
    PORT: str = config("DATABASE_PORT")
    NAME: str = config("DATABASE_NAME")


class API:

    NAME: str = config("API_NAME")
    VERSION: str = config("API_VERSION")
    PREFIX: str = config("API_PREFIX")
    TITLE: str = f"{NAME}: {VERSION}"


class KEYCLOAK:

    URL: str = config("KEYCLOAK_URL")
    TOKEN_URL: str = config("KEYCLOAK_TOKEN_URL")
    USERS_URL: str = config("KEYCLOAK_USERS_URL")
    EXECUTE_ACTIONS_URL: str = config("KEYCLOAK_EXECUTE_ACTIONS_URL")
    REALM: str = config("KEYCLOAK_REALM")
    CLIENT_ID: str = config("KEYCLOAK_CLIENT_ID")
    CLIENT_SECRET: str = config("KEYCLOAK_CLIENT_SECRET")
    USERNAME: str = config("KEYCLOAK_USERNAME")
    PASSWORD: str = config("KEYCLOAK_PASSWORD")
    GRANT_TYPE: str = config("KEYCLOAK_GRANT_TYPE")
    TIMEOUT = int(config("KEYCLOAK_TIMEOUT"))
    CERTS_URL: str = config("KEYCLOAK_CERTS_URL")
    JTW_ALGORITHMS: List[str] = str(config("KEYCLOAK_JTW_ALGORITHMS")).split(",")
    JWT_AUDIENCE: str = config("KEYCLOAK_JWT_AUDIENCE")
    DISABLE_SSL = bool(config("KEYCLOAK_DISABLE_SSL"))
