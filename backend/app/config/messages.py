from app.schemas.utils.error import ErrorContentSchema as Detail

PREFIX = "BID0"


def detail(code: str, message: str):
    return Detail(code=f"{PREFIX}{code}", message=message)


class MESSAGES:
    API_UNEXPECTED_ERROR = detail(
        "0000",
        "Ocurrió un error inesperado",
    )
    SSO_TOKEN_ERROR = detail(
        "0001",
        "Ocurrió un error al obtener el token desde el SSO",
    )
    SSO_CREATE_USER_ERROR = detail(
        "0002",
        "Ocurrió un error al crear el usuario en el SSO",
    )
    SSO_GET_USER_ERROR = detail(
        "0003",
        "Ocurrió un error al obtener el usuario desde el SSO",
    )
    SSO_EXECUTE_ACTION_ERROR = detail(
        "0004",
        "Ocurrió un error al ejecutar una accion en el SSO",
    )
