import { getToken } from "@auth/core/jwt";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  // @ts-ignore: req and salt
  const token = await getToken({ req, secret: process.env.AUTH_SECRET });
  res.json({ token: token?.access_token });
}
