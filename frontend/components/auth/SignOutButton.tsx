import { signOut } from "@app/auth";
import { Button } from "@mui/material";

export function SignOutButton() {
  const getLogOutUri = async () => {
    "use server";
    const base = process.env.AUTH_KEYCLOAK_LOGOUT_URL;
    const redirect = encodeURIComponent(process.env.NEXT_PUBLIC_NEXT_URL);
    const client = process.env.AUTH_KEYCLOAK_ID;
    return `${base}?post_logout_redirect_uri=${redirect}&client_id=${client}`;
  };

  return (
    <form
      action={async () => {
        "use server";
        const redirectTo = await getLogOutUri();
        console.log("REDIRECT URI");
        console.log(redirectTo);
        await signOut({ redirectTo, redirect: true });
      }}
    >
      <Button sx={{ paddingY: 3 }} type="submit">
        Sign Out
      </Button>
    </form>
  );
}
