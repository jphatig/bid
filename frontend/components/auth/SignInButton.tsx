import { signIn } from "@app/auth";
import { Button } from "@mui/material";

export function SignInButton() {
  return (
    <form
      action={async () => {
        "use server";
        await signIn("keycloak");
      }}
    >
      <Button sx={{ paddingY: 3 }} type="submit">
        Sign In
      </Button>
    </form>
  );
}
