import Alert from "@mui/material/Alert";

interface BidSocketAlertProps {
  isConnecting: boolean;
  isOpen: boolean;
}

const BidSocketAlert = ({ isConnecting, isOpen }: BidSocketAlertProps) => {
  const severity = isConnecting ? "info" : isOpen ? "success" : "error";
  const label = isConnecting
    ? "Conectando"
    : isOpen
    ? "Conectado"
    : "Error al conectar";

  return <Alert severity={severity}>{label}</Alert>;
};

export default BidSocketAlert;
