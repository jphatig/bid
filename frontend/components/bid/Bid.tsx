"use client";

import { Box, Button, Grid, MenuItem, TextField } from "@mui/material";

import { useBidWebSocket } from "@app/hooks/websocket";
import { useBids } from "@app/hooks/bid";
import { useCurrentBidder } from "@app/hooks/bidder";
import { getToken } from "@app/providers/token";
import { useState } from "react";
import BidSocketAlert from "./BidSocketAlert";

const Bid = () => {
  const { isOpen, isConnecting, sendBid, bidDetail } = useBidWebSocket();
  const bids = useBids();
  const bidder = useCurrentBidder();

  const [value, setValue] = useState<number>();
  const [bidId, setBidId] = useState<string | undefined>();

  const onSend = async () => {
    if (!value || !bidder || !bidId) return;
    sendBid({
      token: await getToken(),
      detail: {
        bid_id: bidId,
        bidder_id: bidder.id,
        value: value,
      },
    });
  };

  return (
    <Box component="section">
      <BidSocketAlert isOpen={isOpen} isConnecting={isConnecting} />
      <Grid container spacing={2} pt={3}>
        <Grid item xs={6} lg={3}>
          {bidder ? (
            <TextField
              fullWidth
              label="Bidder"
              size="small"
              defaultValue={bidder?.name}
              disabled
            />
          ) : (
            <></>
          )}
        </Grid>
        <Grid item xs={6} lg={3}>
          <TextField
            select
            label="Bid"
            fullWidth
            size="small"
            defaultValue={""}
            onChange={(event) => setBidId(event.target.value)}
          >
            {bids?.map((bid) => (
              <MenuItem key={bid.id} value={bid.id}>
                {bid.title}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <Grid item xs={6} lg={3}>
          <TextField
            fullWidth
            label="Value"
            size="small"
            onChange={(event) =>
              event.target.value ? setValue(Number(event.target.value)) : null
            }
          />
        </Grid>
      </Grid>
      <Grid container spacing={2} pt={5}>
        <Grid item xs={6}>
          <Button variant="text" onClick={onSend} size="small">
            Enviar
          </Button>
        </Grid>
        <Grid item xs={6}>
          The lowest bid value is: {bidDetail?.value || "???"}
        </Grid>
      </Grid>
    </Box>
  );
};

export default Bid;
