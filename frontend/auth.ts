import NextAuth from "next-auth";
import keycloak from "next-auth/providers/keycloak";

export const { handlers, signIn, signOut, auth } = NextAuth({
  providers: [keycloak],
  callbacks: {
    redirect: async ({ url, baseUrl }) => {
      if (url.startsWith(process.env.AUTH_KEYCLOAK_ISSUER)) return url;
      else if (new URL(url).origin === baseUrl) return url;
      return baseUrl;
    },
    jwt({ token, user, account }) {
      if (user) {
        token.id = user.id;
        token.access_token = account?.access_token;
      }
      return token;
    },
    session({ session, token }: any) {
      session.user.id = token.id;
      session.access_token = token.access_token;
      return session;
    },
  },
});
