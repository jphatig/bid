export const getToken = async () => {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_NEXT_URL}/api/auth/token`
  );
  const data: TokenResponse = await response.json();
  return data.token;
};
