import { useCallback, useEffect, useState } from "react";
import { Bid } from "@app/interfaces/bid";

export const useBids = () => {
  const [bids, setBids] = useState<Bid[]>([]);

  const fetchBids = async () => {
    const url = `${process.env.NEXT_PUBLIC_API_URL}/api/v1/bid`;
    const response = await fetch(url);
    const data = await response.json();
    setBids(data);
  };

  useEffect(
    useCallback(() => {
      fetchBids();
    }, []),
    []
  );

  return bids;
};
