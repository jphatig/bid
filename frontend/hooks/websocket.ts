import { BidDetail, BidSocketMessage } from "@app/interfaces/bid";
import { useEffect, useState } from "react";
import useWebSocket, { ReadyState } from "react-use-websocket";

interface BidWebSocketHook {
  isOpen: boolean;
  isConnecting: boolean;
  sendBid: (message: BidSocketMessage) => void;
  bidDetail?: BidDetail;
}

export const useBidWebSocket = (): BidWebSocketHook => {
  const { sendJsonMessage, lastJsonMessage, readyState } = useWebSocket(
    process.env.NEXT_PUBLIC_WS_URL,
    {
      share: true,
      shouldReconnect: () => true,
    }
  );
  const [bidDetail, setBidDetail] = useState<BidDetail>();

  const isOpen = readyState === ReadyState.OPEN;
  const isConnecting = readyState === ReadyState.CONNECTING;

  const sendBid = async (message: BidSocketMessage) => {
    if (!isOpen) return;
    sendJsonMessage(message);
  };

  useEffect(() => {
    setBidDetail(JSON.parse(lastJsonMessage));
  }, [lastJsonMessage]);

  return { isOpen, isConnecting, sendBid, bidDetail };
};
