import { Bidder } from "@app/interfaces/bid";
import { getToken } from "@app/providers/token";
import { useCallback, useEffect, useState } from "react";

export const useCurrentBidder = () => {
  const [bidder, setBidder] = useState<Bidder>();

  const getBidder = async () => {
    const token = await getToken();
    if (!token) return;
    const url = `${process.env.NEXT_PUBLIC_API_URL}/api/v1/bidder/me`;
    const headers = {
      Authorization: `Bearer ${token}`,
    };
    const response = await fetch(url, { headers });
    const data = await response.json();
    setBidder(data);
  };

  useEffect(
    useCallback(() => {
      getBidder();
    }, []),
    []
  );
  return bidder;
};
