export interface Bidder {
  id: string;
  name: string;
  user_id: string;
}

export interface Bid {
  id: string;
  title: string;
  value: string;
  start_date: string;
  end_date: string;
  winner_id?: string;
  won_value?: number;
  created_at: string;
}

export interface BidSocketMessage {
  token: string;
  detail: BidDetail;
}

export interface BidDetail {
  bid_id: string;
  bidder_id: string;
  value: number;
  created_at?: string;
}
