import { SignInButton } from "@app/components/auth/SignInButton";
import { SignOutButton } from "@app/components/auth/SignOutButton";
import { auth } from "@app/auth";
import Bid from "@app/components/bid/Bid";
import { Box, Container } from "@mui/material";

const AbcPage = async () => {
  const session = await auth();

  return (
    <Container>
      {session?.user ? (
        <Box>
          <SignOutButton />
          <Bid />
        </Box>
      ) : (
        <SignInButton />
      )}
    </Container>
  );
};

export default AbcPage;
