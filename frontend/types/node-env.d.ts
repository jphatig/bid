// types/node-env.d.ts
declare namespace NodeJS {
  export interface ProcessEnv {
    AUTH_KEYCLOAK_ID: string;
    AUTH_KEYCLOAK_SECRET: string;
    AUTH_KEYCLOAK_ISSUER: string;
    AUTH_KEYCLOAK_LOGOUT_URL: string;
    AUTH_SECRET: string;
    NEXT_PUBLIC_WS_URL: string;
    NEXT_PUBLIC_API_URL: string;
    NEXT_PUBLIC_NEXT_URL: string;
  }
}
